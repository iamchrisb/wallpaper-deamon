package de.home.wallpaperdeamon;

import com.sun.jna.Platform;
import de.home.wallpaperdeamon.app.control.TrayActivity;
import de.home.wallpaperdeamon.app.view.TrayView;
import de.home.wallpaperdeamon.app.view.TrayViewImpl;
import de.home.wallpaperdeamon.util.Constants;
import de.home.wallpaperdeamon.util.Environment;
import java.io.File;
import java.io.IOException;
import javafx.application.Application;
import javafx.stage.Stage;

public class WallpaperApp extends Application {

  public static void main(String[] args) throws IOException {
    hideApplicationFromDockOnMacOS();
    launch(args);
  }

  /** this method must be the first method to call! */
  private static void hideApplicationFromDockOnMacOS() {
    if (Platform.isMac()) {
      // TODO its not working anymore because of JavaFX
      //      System.setProperty("apple.awt.UIElement", "true");
      //      com.sun.javafx.application.PlatformImpl.setTaskbarApplication(false);
    }
  }

  private void prepareFileSystem() throws IOException {
    createDirIfNotExists(Constants.JWP_TMP_DIR);
    createDirIfNotExists(Constants.JWP_DEFAULT_OFFLINE_WALLPAPER_DIR);
    createDirIfNotExists(Constants.JWP_TMP_DOWNLOADS_DIR);
    createFileIfNotExists(Constants.JWP_SETTINGS_FILE);
  }

  private static void createFileIfNotExists(String filePath) throws IOException {
    File file = new File(filePath);
    if (!file.exists()) {
      file.createNewFile();
      file.setWritable(true);
      file.setReadable(true);
    }
  }

  private static void createDirIfNotExists(String folderPath) {
    File directory = new File(folderPath);
    if (!directory.exists()) {
      directory.mkdir();
    }
  }

  private void initApplication(Stage stage) throws IOException {
    final TrayView trayView = new TrayViewImpl(stage);
    TrayActivity trayActivity = new TrayActivity(trayView);
    trayView.setPresenter(trayActivity);
  }

  private void verifyEnvironmentVariables() {
    Environment.getUnsplashApiSecret();
    Environment.getUnsplashClientId();
  }

  @Override
  public void start(Stage stage) throws Exception {
    verifyEnvironmentVariables();
    prepareFileSystem();
    initApplication(stage);
  }
}
