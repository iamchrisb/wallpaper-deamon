package de.home.wallpaperdeamon.util;

public class Environment {
    public static final String UNSPLASH_CLIENT_ID = "UNSPLASH_CLIENT_ID";
    public static final String UNSPLASH_API_SECRET = "UNSPLASH_API_SECRET";

    public static String getUnsplashClientId() {
        String clientId = System.getenv(UNSPLASH_CLIENT_ID);
        if(clientId == null) {
            throw new IllegalArgumentException("UNSPLASH_API_SECRET must be set!");
        }
        return clientId;
    }

    public static String getUnsplashApiSecret() {
        String apiSecret = System.getenv(UNSPLASH_API_SECRET);
        if(apiSecret == null) {
            throw new IllegalArgumentException("UNSPLASH_API_SECRET must be set!");
        }
        return apiSecret;
    }
}
