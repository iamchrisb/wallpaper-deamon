package de.home.wallpaperdeamon.util;

import java.io.File;

public class Constants {

  /** file extensions */
  public static final String JPG = ".jpg";

  public static final String JPEG = ".jpeg";

  /** time constants */
  public static final int SECOND = 1000;

  public static final int MINUTE = SECOND * 60;
  public static final int DEFAULT_PERIOD = MINUTE * 10;
  public static final int DEFAULT_DELAY = MINUTE * 10;

  /** 100 MB */
  public static final long DEFAULT_MAX_DOWNLOAD_FOLDER_SIZE_IN_BYTES = 100 * 1000000;

  /** paths */
  public static final String WORKING_DIR = System.getProperty("user.dir");

  public static final String RES_DIR = WORKING_DIR + File.separator + "resources";
  public static final String IMAGES_DIR = RES_DIR + File.separator + "images";
  public static final String TRAY_ICON_PATH = IMAGES_DIR + File.separator + "icon_light.png";

  public static final String HOME_DIR = System.getProperty("user.home") + File.separator;
  public static final String JWP_TMP_DIR = HOME_DIR + "jwp" + File.separator;
  public static final String JWP_DEFAULT_OFFLINE_WALLPAPER_DIR =
      JWP_TMP_DIR + "offline" + File.separator;
  public static final String JWP_TMP_DOWNLOADS_DIR = JWP_TMP_DIR + "downloads" + File.separator;
  public static final String JWP_SETTINGS_FILE = JWP_TMP_DIR + "jwp.settings";

  /** strings */
  public static final String MENU_ITEM_NEXT = "Next";

  public static final String SET_WALLPAPER_FOLDER_PATH = "Set wallpaper folder path";
  public static final String INSERT_FOLDER_PATH = "Insert folder path";
  public static final String SHOW_CURRENT_PATH = "Show current path";
  public static final String MENU_ITEM_PAUSE = "Pause";
  public static final String MENU_ITEM_KILL = "Exit";
  public static final String MENU_ITEM_START = "Start";
  public static final String INTERVAL = "Interval in minutes";
  public static final String MENU_ITEM_MODE = "Online Mode";
  public static final String MENU_ITEM_RUNNING = "Runnning";
  public static final String MENU_ITEM_MORE_SETTINGS = "More Settings ^";
}
