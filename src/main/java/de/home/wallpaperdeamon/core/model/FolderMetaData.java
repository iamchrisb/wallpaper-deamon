package de.home.wallpaperdeamon.core.model;

import lombok.Data;

@Data
public class FolderMetaData {
  private double sizeInBytes;
  private double sizeInMegaBytes;
  private int imageCount;
  private int fileCount;
}
