package de.home.wallpaperdeamon.core;

import de.home.wallpaperdeamon.util.Constants;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;

public class LocalSettingsManager {
  public static final String MAX_DOWNLOAD_FOLDER_SIZE_KEY = "resource.sizes.folders.download";
  public static String OFFLINE_WALLPAPER_KEY = "resource.offline.wallpaper.path";
  public static String ONLINE_MODE_KEY = "resource.mode.online";

  private Properties appProps;

  public LocalSettingsManager() throws IOException {
    appProps = new Properties();
    appProps.load(new FileInputStream(Constants.JWP_SETTINGS_FILE));

    try {
      getProperty(ONLINE_MODE_KEY);
    } catch (IllegalArgumentException e) {
      setProperty(ONLINE_MODE_KEY, true);
    }
  }

  public String getProperty(String key) {
    String currentProp = appProps.getProperty(key);
    if (currentProp == null || currentProp.isEmpty()) {
      throw new IllegalArgumentException("Property for key: " + key + " is not defined!");
    }
    return currentProp;
  }

  public Boolean getPropertyAsBoolean(String key) {
    return new Boolean(getProperty(key));
  }

  public Long getPropertyAsLong(String key) {
    return new Long(getProperty(key));
  }

  public void setProperty(String key, String value) {
    appProps.setProperty(key, value);
    save();
  }

  public void setProperty(String key, Boolean value) {
    setProperty(key, value.toString());
  }

  public void setProperty(String key, Long value) {
    setProperty(key, value.toString());
  }

  public void save() {
    FileOutputStream fos = null;
    try {
      fos = new FileOutputStream(Constants.JWP_SETTINGS_FILE);
      appProps.store(fos, null);
    } catch (IOException e) {
      e.printStackTrace();
    } finally {
      if (fos != null) {
        try {
          fos.close();
        } catch (IOException e) {
          e.printStackTrace();
        }
      }
    }
  }
}
