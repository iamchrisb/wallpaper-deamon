package de.home.wallpaperdeamon.core;

import de.home.wallpaperdeamon.core.helper.WallpaperHelper;
import de.home.wallpaperdeamon.util.Constants;
import java.io.File;
import java.io.IOException;
import java.util.Timer;
import java.util.TimerTask;

public class DesktopManager {

  private long delay;
  private long period;
  private ResourceManager resourceManager;
  private WallpaperHelper wallpaperHelper;
  private Timer timer;
  private long delayInMs;

  public DesktopManager(ResourceManager resourceManager, WallpaperHelper wallpaperHelper) {
    this.resourceManager = resourceManager;
    this.wallpaperHelper = wallpaperHelper;
    timer = new Timer();
    this.delay = Constants.DEFAULT_DELAY;
    this.period = Constants.DEFAULT_PERIOD;
  }

  public void rand() {
    timer = new Timer();
    timer.schedule(
        new TimerTask() {
          @Override
          public void run() {
            try {
              File randomNextImage = resourceManager.getRandomImage();
              wallpaperHelper.setWallpaper(randomNextImage.getAbsolutePath());
            } catch (IOException e) {
              e.printStackTrace();
            }
          }
        },
        this.delay,
        this.period);
  }

  public void next() throws IOException {
    File nextImage = resourceManager.getNextImage();
    wallpaperHelper.setWallpaper(nextImage.getAbsolutePath());
  }

  public void setPeriod(long periodInMs) {
    this.period = periodInMs;
  }

  public void setDelay(long delayInMs) {
    this.delayInMs = delayInMs;
  }

  public void stop() {
    timer.cancel();
    timer.purge();
  }
}
