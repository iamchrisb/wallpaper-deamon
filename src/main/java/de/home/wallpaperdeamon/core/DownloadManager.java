package de.home.wallpaperdeamon.core;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

public class DownloadManager {
  public void downloadImage(String imageUrl, String destinationFilePath) throws IOException {
    InputStream inputStream = new URL(imageUrl).openStream();
    FileOutputStream fileOutputStream = new FileOutputStream(destinationFilePath);

    byte[] bytes = new byte[4096];
    int length;

    while ((length = inputStream.read(bytes)) != -1) {
      fileOutputStream.write(bytes, 0, length);
    }

    inputStream.close();
    fileOutputStream.close();
  }

  public File downloadImageReturnFile(String imageUrl, String destinationFilePath)
      throws IOException {
    downloadImage(imageUrl, destinationFilePath);
    return new File(destinationFilePath);
  }
}
