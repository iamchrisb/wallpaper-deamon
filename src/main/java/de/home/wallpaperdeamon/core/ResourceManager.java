package de.home.wallpaperdeamon.core;

import de.home.wallpaperdeamon.app.InstanceContainer;
import de.home.wallpaperdeamon.core.model.FolderMetaData;
import de.home.wallpaperdeamon.unsplash.UnsplashApiClient;
import de.home.wallpaperdeamon.unsplash.UnsplashOrientation;
import de.home.wallpaperdeamon.unsplash.model.UnsplashImage;
import de.home.wallpaperdeamon.util.Constants;
import de.home.wallpaperdeamon.util.Environment;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Random;

public class ResourceManager {

  private File currentFile;
  private File[] wallpapers;

  private ResourceMode mode = ResourceMode.ONLINE;
  private String folderPath;
  private LocalSettingsManager localSettingsManager;

  public ResourceManager(String folderPath) {
    this.folderPath = folderPath;
    localSettingsManager = InstanceContainer.getInstance().getLocalSettingsManager();

    setDefaultMaxDownloadFolderSize();
  }

  private void setDefaultMaxDownloadFolderSize() {
    try {
      localSettingsManager.getProperty(LocalSettingsManager.MAX_DOWNLOAD_FOLDER_SIZE_KEY);
    } catch (Exception e) {
      localSettingsManager.setProperty(
          LocalSettingsManager.MAX_DOWNLOAD_FOLDER_SIZE_KEY,
          Constants.DEFAULT_MAX_DOWNLOAD_FOLDER_SIZE_IN_BYTES);
    }
  }

  public void setMode(ResourceMode mode) {
    this.mode = mode;

    InstanceContainer.getInstance()
        .getLocalSettingsManager()
        .setProperty(LocalSettingsManager.ONLINE_MODE_KEY, mode.equals(ResourceMode.ONLINE));
  }

  public File getNextImage() throws IOException {
    if (mode.equals(ResourceMode.ONLINE)) {
      return getOnlineImage();
    } else {
      return getOnlineImage();
    }
  }

  public FolderMetaData getOnlineDownloadFolderInfo() {
    try {
      return getInfo(Constants.JWP_TMP_DOWNLOADS_DIR);
    } catch (IOException e) {
      e.printStackTrace();
    }
    return null;
  }

  public FolderMetaData getInfo(String folderPath) throws IOException {
    FolderMetaData folderMetaData = new FolderMetaData();

    Path folder = Paths.get(folderPath);
    double size =
        Files.walk(folder)
            .filter(p -> p.toFile().isFile())
            .mapToLong(p -> p.toFile().length())
            .sum();

    folderMetaData.setSizeInBytes(size);
    folderMetaData.setSizeInMegaBytes(size / 1000000);

    File file = new File(folderPath);
    folderMetaData.setFileCount(file.listFiles().length);

    return folderMetaData;
  }

  private File getOnlineImage() throws IOException {
    UnsplashApiClient unsplashApiClient = new UnsplashApiClient(Environment.getUnsplashClientId());
    UnsplashImage wallpaper =
        unsplashApiClient.getRandomPhotoByQuery(
            "wallpaper", true, null, UnsplashOrientation.LANDSCAPE);
    String imageUrl = wallpaper.getUrls().get("full");

    DownloadManager downloadManager = new DownloadManager();

    String filePath = Constants.JWP_TMP_DOWNLOADS_DIR + wallpaper.getId() + Constants.JPEG;
    downloadManager.downloadImage(imageUrl, filePath);
    return new File(filePath);
  }

  private void setUpOfflineImages(final String folderPath) {
    this.currentFile = new File(folderPath);

    if (!currentFile.exists()) {
      throw new IllegalArgumentException("folder does not exist");
    }

    if (!currentFile.isDirectory()) {
      throw new IllegalArgumentException("given path is no folder");
    }

    final File[] files =
        currentFile.listFiles(
            pathname -> {
              final String absolutePath = pathname.getAbsolutePath();
              if (absolutePath.contains(Constants.JPG) || absolutePath.contains(Constants.JPEG)) {
                return true;
              }
              return false;
            });

    this.wallpapers = files;
  }

  private File getRandomOfflineImage() {
    return wallpapers[new Random().nextInt(wallpapers.length)];
  }

  public File getRandomImage() throws IOException {
    if (this.mode.equals(ResourceMode.ONLINE)) {
      return getOnlineImage();
    } else {
      return getOnlineImage();
    }
  }

  public String getOfflinePath() {
    return this.folderPath;
  }

  public void setOfflinePath(String folderPath) {
    this.folderPath = folderPath;
  }
}
