package de.home.wallpaperdeamon.core.helper;

import com.sun.jna.Platform;
import de.home.wallpaperdeamon.User32;
import java.io.IOException;

public class WallpaperHelper {

  public void setWallpaper(final String wallpaperPath) {
    if (Platform.isWindows()) {
      setWallpaperWin(wallpaperPath);
    } else if (Platform.isMac()) {
      setWallpaperMac(wallpaperPath);
    } else {
      throw new IllegalArgumentException("unsupported OS");
    }
  }

  private void setWallpaperMac(final String wallpaperPath) {
    Runtime runtime = Runtime.getRuntime();
    String[] args = {
      "osascript",
      "-e",
      "tell application \"Finder\" to set desktop picture to POSIX file \"" + wallpaperPath + "\""
    };

    try {
      Process process = runtime.exec(args);
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  private void setWallpaperWin(final String wallpaperPath) {
    User32.INSTANCE.SystemParametersInfo(
        User32.SPI_SETDESKWALLPAPER,
        0,
        wallpaperPath,
        User32.SPIF_SENDWININICHANGE | User32.SPIF_UPDATEINIFILE);
  }
}
