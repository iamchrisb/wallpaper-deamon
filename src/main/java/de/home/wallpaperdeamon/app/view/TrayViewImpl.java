package de.home.wallpaperdeamon.app.view;

import de.home.wallpaperdeamon.app.settings.SettingsView;
import de.home.wallpaperdeamon.core.ResourceMode;
import de.home.wallpaperdeamon.util.Constants;
import javafx.application.Platform;
import javafx.stage.Stage;

import javax.swing.*;
import java.awt.*;
import java.io.IOException;

public class TrayViewImpl implements TrayView {

  private MenuItem pauseItem;
  private MenuItem nextButton;
  private MenuItem startItem;
  private MenuItem showCurrentPath;
  MenuItem setWallpaperPathItem;
  private Stage stage;

  private Presenter presenter;
  private CheckboxMenuItem statusCheckbox;
  private SettingsView settingsView;

  public TrayViewImpl(Stage stage) throws IOException {
    this.stage = stage;
    if (!SystemTray.isSupported()) {
      throw new IllegalArgumentException("SystemTray is not supported!");
    }

    settingsView = new SettingsView(stage);
    //    stage.show();

    PopupMenu trayPopupMenu = new PopupMenu();
    addModeMenuItem(trayPopupMenu);

    addStatusMenuItem(trayPopupMenu);

    // ----------
    trayPopupMenu.addSeparator();

    addNextMenuItem(trayPopupMenu);
    addMoreSettingsMenuItem(trayPopupMenu);

    // ------------
    trayPopupMenu.addSeparator();

    addStartMenuItem(trayPopupMenu);
    addPauseMenuItem(trayPopupMenu);

    // ----------
    //    trayPopupMenu.addSeparator();
    //    addSetWallpaperPathMenuItem(trayPopupMenu);
    //    addShowCurrentPathMenuItem(trayPopupMenu);

    // ----------
    trayPopupMenu.addSeparator();

    MenuItem close = new MenuItem(Constants.MENU_ITEM_KILL);
    close.addActionListener(e -> System.exit(0));
    trayPopupMenu.add(close);

    setTrayIcon(trayPopupMenu);
  }

  private void addStatusMenuItem(PopupMenu trayPopupMenu) {
    statusCheckbox = new CheckboxMenuItem();
    statusCheckbox.setLabel(Constants.MENU_ITEM_RUNNING);
    trayPopupMenu.add(statusCheckbox);
  }

  private void addSetWallpaperPathMenuItem(PopupMenu trayPopupMenu) {

    setWallpaperPathItem = new MenuItem(Constants.SET_WALLPAPER_FOLDER_PATH);
    trayPopupMenu.add(setWallpaperPathItem);
    setWallpaperPathItem.addActionListener(
        e -> {
          JOptionPane.showInputDialog(Constants.INSERT_FOLDER_PATH);
        });
  }

  private void addShowCurrentPathMenuItem(PopupMenu trayPopupMenu) {
    showCurrentPath = new MenuItem(Constants.SHOW_CURRENT_PATH);
    trayPopupMenu.add(showCurrentPath);
    showCurrentPath.addActionListener(
        e -> {
          if (presenter == null) {
            return;
          }
          JOptionPane.showMessageDialog(null, presenter.getCurrentFolderPath());
        });
  }

  private void addStartMenuItem(PopupMenu trayPopupMenu) {
    startItem = new MenuItem(Constants.MENU_ITEM_START);
    startItem.addActionListener(
        e -> {
          if (presenter == null) {
            return;
          }
          statusCheckbox.setState(true);
          presenter.start();
          startItem.setEnabled(false);
          pauseItem.setEnabled(true);
        });
    trayPopupMenu.add(startItem);
  }

  private void addPauseMenuItem(PopupMenu trayPopupMenu) {
    pauseItem = new MenuItem(Constants.MENU_ITEM_PAUSE);
    pauseItem.addActionListener(
        e -> {
          statusCheckbox.setState(false);
          presenter.stop();
          startItem.setEnabled(true);
          pauseItem.setEnabled(false);
        });
    trayPopupMenu.add(pauseItem);
    pauseItem.setEnabled(false);
  }

  private void addNextMenuItem(PopupMenu trayPopupMenu) {
    nextButton = new MenuItem(Constants.MENU_ITEM_NEXT);
    nextButton.addActionListener(
        e -> {
          presenter.next();
        });
    trayPopupMenu.add(nextButton);
  }

  private void addMoreSettingsMenuItem(PopupMenu trayPopupMenu) {
    MenuItem settingsMenuItem = new MenuItem(Constants.MENU_ITEM_MORE_SETTINGS);
    trayPopupMenu.add(settingsMenuItem);
    settingsMenuItem.addActionListener(
        e -> {
          Platform.runLater(() -> stage.show());
        });
  }

  private void addModeMenuItem(PopupMenu trayPopupMenu) {
    CheckboxMenuItem resourceModeMenuItem = new CheckboxMenuItem();
    resourceModeMenuItem.setLabel(Constants.MENU_ITEM_MODE);
    resourceModeMenuItem.setState(true);
    trayPopupMenu.add(resourceModeMenuItem);
    resourceModeMenuItem.addItemListener(
        e -> {
          if (presenter == null) {
            return;
          }

          if (resourceModeMenuItem.getState()) {
            presenter.setResourceMode(ResourceMode.ONLINE);
          } else {
            presenter.setResourceMode(ResourceMode.OFFLINE);
          }
        });
  }

  private void setTrayIcon(PopupMenu trayPopupMenu) throws IOException {
    SystemTray systemTray = SystemTray.getSystemTray();
    Image image = null;

    try {
      image = Toolkit.getDefaultToolkit().getImage(Constants.TRAY_ICON_PATH);
    } catch (Exception e) {
      throw new IOException("tray icon could not be loaded");
    }

    final String tooltip = "Java Wallpaper Deamon" + " | Running: " + statusCheckbox.getState();

    TrayIcon trayIcon = new TrayIcon(image, tooltip, trayPopupMenu);
    trayIcon.setImageAutoSize(true);

    try {
      systemTray.add(trayIcon);
    } catch (AWTException awtException) {
      awtException.printStackTrace();
    }
  }

  @Override
  public void setCurrentFolderSize(double currentSizeInMb, double maxSizeInMb) {
    settingsView.showDownloadFolderSize(currentSizeInMb, maxSizeInMb);
  }

  @Override
  public void setPresenter(final Presenter presenter) {
    this.presenter = presenter;
  }
}
