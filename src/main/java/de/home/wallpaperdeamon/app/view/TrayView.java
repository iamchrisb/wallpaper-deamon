package de.home.wallpaperdeamon.app.view;

import de.home.wallpaperdeamon.core.ResourceMode;

public interface TrayView {

  void setCurrentFolderSize(double currentSizeInMb, double maxSizeInMb);

  interface Presenter {
    void onChoosePathSelected(String path);

    String getCurrentFolderPath();

    void start();

    void next();

    void stop();

    void setResourceMode(ResourceMode mode);
  }

  void setPresenter(Presenter presenter);
}
