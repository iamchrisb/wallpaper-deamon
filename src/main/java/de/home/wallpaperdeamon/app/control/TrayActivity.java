package de.home.wallpaperdeamon.app.control;

import de.home.wallpaperdeamon.app.InstanceContainer;
import de.home.wallpaperdeamon.app.view.TrayView;
import de.home.wallpaperdeamon.app.widgets.PathChooserWidget;
import de.home.wallpaperdeamon.core.DesktopManager;
import de.home.wallpaperdeamon.core.LocalSettingsManager;
import de.home.wallpaperdeamon.core.ResourceManager;
import de.home.wallpaperdeamon.core.ResourceMode;
import de.home.wallpaperdeamon.core.helper.WallpaperHelper;
import de.home.wallpaperdeamon.core.model.FolderMetaData;
import de.home.wallpaperdeamon.util.Constants;

import java.io.IOException;

public class TrayActivity implements TrayView.Presenter {

  private final DesktopManager desktopManager;
  private final ResourceManager resourceManager;

  private TrayView view;

  public TrayActivity(final TrayView view) {
    this.view = view;
    this.resourceManager = new ResourceManager(Constants.JWP_DEFAULT_OFFLINE_WALLPAPER_DIR);
    desktopManager = new DesktopManager(resourceManager, new WallpaperHelper());

    FolderMetaData info = resourceManager.getOnlineDownloadFolderInfo();

    long maxDownloadFolderSizeInBytes =
        InstanceContainer.getInstance()
            .getLocalSettingsManager()
            .getPropertyAsLong(LocalSettingsManager.MAX_DOWNLOAD_FOLDER_SIZE_KEY);

    long sizeInMb = maxDownloadFolderSizeInBytes / 1000000;
    view.setCurrentFolderSize(info.getSizeInMegaBytes(), sizeInMb);
  }

  public TrayActivity(final TrayView view, String folderPath) {
    this.view = view;
    resourceManager = new ResourceManager(folderPath);
    desktopManager = new DesktopManager(resourceManager, new WallpaperHelper());
  }

  @Override
  public void onChoosePathSelected(final String path) {
    final PathChooserWidget pathChooserWidget = new PathChooserWidget();
  }

  @Override
  public String getCurrentFolderPath() {
    return resourceManager.getOfflinePath();
  }

  @Override
  public void start() {
    desktopManager.rand();
  }

  @Override
  public void next() {
    try {
      desktopManager.next();
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  @Override
  public void stop() {
    this.desktopManager.stop();
  }

  @Override
  public void setResourceMode(ResourceMode mode) {
    resourceManager.setMode(mode);
  }
}
