package de.home.wallpaperdeamon.app;

import de.home.wallpaperdeamon.core.LocalSettingsManager;

import java.io.IOException;

public class InstanceContainer {

  private static InstanceContainer instance;

  LocalSettingsManager localSettingsManager;

  private InstanceContainer() {
    try {
      localSettingsManager = new LocalSettingsManager();
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  public static InstanceContainer getInstance() {
    if (instance == null) {
      instance = new InstanceContainer();
    }
    return instance;
  }

  public LocalSettingsManager getLocalSettingsManager() {
    return localSettingsManager;
  }
}
