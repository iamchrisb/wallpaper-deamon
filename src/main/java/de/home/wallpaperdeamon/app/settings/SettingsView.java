package de.home.wallpaperdeamon.app.settings;

import de.home.wallpaperdeamon.util.Constants;
import javafx.application.Platform;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

import java.awt.*;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Stream;

public class SettingsView {
  private ImageView previewImageView;
  int heightOffset = 100;
  int currentWidth;
  int currentHeight;

  StackPane root;
  HBox headerContainer;

  public SettingsView(Stage stage) {
    Platform.setImplicitExit(false);

    root = new StackPane();

    headerContainer = new HBox(10);
    root.getChildren().add(headerContainer);

    Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();

    currentWidth = screenSize.width / 2;
    currentHeight = screenSize.height / 2;

    previewImageView = new ImageView();
    setDefaultPreviewImage();

    HBox imageContainer = new HBox(previewImageView);
    imageContainer.setAlignment(Pos.CENTER);
    imageContainer.setPadding(new Insets(20));

    HBox thumbsAndControlsContainer = new HBox();
    thumbsAndControlsContainer.setAlignment(Pos.CENTER);

    HBox imageSliderContainer = new HBox();
    imageSliderContainer.setAlignment(Pos.CENTER);
    addImageSlider(imageSliderContainer);

    Button nextBtn = new Button(">");
    Button prevBtn = new Button("<");

    thumbsAndControlsContainer.getChildren().addAll(prevBtn, imageSliderContainer, nextBtn);

    Pane container = new VBox(imageContainer, thumbsAndControlsContainer);
    root.getChildren().add(container);

    Scene scene = new Scene(root, currentWidth, currentHeight);
    stage.setTitle("JWP - a wallpaper tool for all operating systems");
    stage.setScene(scene);
  }

  private void setDefaultPreviewImage() {
    try {
      File[] files = new File(Constants.JWP_TMP_DOWNLOADS_DIR).listFiles();
      FileInputStream fis = new FileInputStream(files[0]);
      Image image = new Image(fis, currentWidth, currentHeight - heightOffset, true, true);
      previewImageView.setImage(image);
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  private void addImageSlider(Pane imageSliderContainer) {
    List<Pane> thumbNails = new LinkedList<>();

    File[] imageSources = new File(Constants.JWP_TMP_DOWNLOADS_DIR).listFiles();
    Stream.of(imageSources)
        .limit(5)
        .forEach(
            file -> {
              try {
                Pane pane = new Pane();
                pane.setPadding(new Insets(5));
                Image currentImage = new Image(new FileInputStream(file), 100, 100, true, true);
                ImageView currentImageView = new ImageView(currentImage);

                currentImageView.setOnMouseClicked(
                    e -> {
                      ImageView imageView = (ImageView) e.getSource();
                      Image image = imageView.getImage();
                      previewImageView.setImage(image);
                    });

                pane.getChildren().add(currentImageView);
                thumbNails.add(pane);
              } catch (FileNotFoundException e) {
                e.printStackTrace();
              }
            });

    imageSliderContainer.getChildren().addAll(thumbNails);
  }

  public void showDownloadFolderSize(double currentSizeInMb, double maxSizeInMb) {
    double percentage = currentSizeInMb / maxSizeInMb;

    HBox folderSizeContainer = new HBox();
    ProgressBar progressBar = new ProgressBar(percentage);
    Label folderSizeLabel = new Label("Folder-size: ");
    folderSizeContainer.getChildren().addAll(folderSizeLabel, progressBar);

    headerContainer.getChildren().add(folderSizeContainer);
  }
}
