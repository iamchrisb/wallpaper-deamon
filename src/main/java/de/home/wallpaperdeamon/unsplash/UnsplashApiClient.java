package de.home.wallpaperdeamon.unsplash;

import com.fasterxml.jackson.databind.ObjectMapper;
import de.home.wallpaperdeamon.unsplash.model.UnsplashCollectionSearchResult;
import de.home.wallpaperdeamon.unsplash.model.UnsplashImage;
import de.home.wallpaperdeamon.unsplash.model.UnsplashPhotoSearchResult;

import java.io.IOException;
import java.net.URL;

public class UnsplashApiClient {

  private static final int DEFAULT_PAGE = 1;
  private static final int DEFAULT_PER_PAGE = 10;
  private static final int MAX_RESULTS_PER_PAGE = 30;
  private static final int MIN_RESULTS_PER_PAGE = 1;
  private String domain = "api.unsplash.com/";
  private String protocol = "https://";

  private String photos = "photos/";
  private String collections = "collections/";
  private String search = "search/";
  private String random = "random/";

  private String apiUrl = protocol + domain;
  private String photosUrl = apiUrl + photos;
  private String randomPhotoUrl = photosUrl + random;
  private String collectionsUrl = apiUrl + collections;

  private String searchPhotos = apiUrl + search + photos;
  private String searchCollections = apiUrl + search + collections;

  private String clientId;
  private final ObjectMapper objectMapper;

  public UnsplashApiClient(final String clientId) {
    this.clientId = clientId;
    if (clientId == null || clientId.isEmpty()) {
      throw new IllegalArgumentException("ClientID cannot be null or empty");
    }
    objectMapper = new ObjectMapper();
  }

  /** get photo */
  public UnsplashImage getPhoto(String id) throws IOException {
    StringBuilder sb = new StringBuilder(photosUrl);
    sb.append(id);
    sb.append("?client_id=" + clientId);

    return objectMapper.readValue(new URL(sb.toString()), UnsplashImage.class);
  }

  /** get photos */
  public UnsplashImage[] getPhotos() throws IOException {
    return getPhotos(DEFAULT_PAGE, DEFAULT_PER_PAGE, UnsplashSort.LATEST);
  }

  public UnsplashImage[] getPhotos(int page) throws IOException {
    return getPhotos(page, DEFAULT_PER_PAGE, UnsplashSort.LATEST);
  }

  public UnsplashImage[] getPhotos(int page, UnsplashSort orderBy) throws IOException {
    return getPhotos(page, DEFAULT_PER_PAGE, orderBy);
  }

  public UnsplashImage[] getPhotos(int page, int perPage) throws IOException {
    return getPhotos(page, perPage, UnsplashSort.LATEST);
  }

  public UnsplashImage[] getPhotos(int page, int perPage, UnsplashSort sort) throws IOException {
    URL photosUrl = new URL(this.photosUrl + "?client_id=" + clientId);
    return objectMapper.readValue(photosUrl, UnsplashImage[].class);
  }

  /** get random photos */
  public UnsplashImage getRandomPhotoByQuery(
      String query, boolean featured, String username, UnsplashOrientation orientation)
      throws IOException {
    StringBuilder sb = new StringBuilder(randomPhotoUrl);
    sb.append("?client_id=" + clientId);

    if (query != null && !query.isEmpty()) {
      sb.append("&query=" + query);
    }

    sb.append("&featured=" + featured);

    if (orientation != null) {
      sb.append("&orientation=" + orientation.getValue());
    }

    if (username != null && !username.isEmpty()) {
      sb.append("&username=" + username);
    }

    return objectMapper.readValue(new URL(sb.toString()), UnsplashImage.class);
  }

  public UnsplashImage getRandomPhotoByCollections(
      String[] collections, boolean featured, String username, UnsplashOrientation orientation) {
    throw new IllegalArgumentException("this method is not supported yet");
  }

  /** search photos */
  public UnsplashPhotoSearchResult searchPhotos(String query) throws IOException {
    return searchPhotos(query, DEFAULT_PAGE, DEFAULT_PER_PAGE, null, null);
  }

  public UnsplashPhotoSearchResult searchPhotos(String query, String[] collections)
      throws IOException {
    return searchPhotos(query, DEFAULT_PAGE, DEFAULT_PER_PAGE, null, collections);
  }

  public UnsplashPhotoSearchResult searchPhotos(
      String query, UnsplashOrientation orientation, String[] collections) throws IOException {
    return searchPhotos(query, DEFAULT_PAGE, DEFAULT_PER_PAGE, orientation, collections);
  }

  public UnsplashPhotoSearchResult searchPhotos(
      String query, int page, int perPage, String[] collections) throws IOException {
    return searchPhotos(query, page, perPage, null, collections);
  }

  public UnsplashPhotoSearchResult searchPhotos(String query, int page, int perPage)
      throws IOException {
    return searchPhotos(query, page, perPage, null, null);
  }

  public UnsplashPhotoSearchResult searchPhotos(String query, UnsplashOrientation orientation)
      throws IOException {
    return searchPhotos(query, DEFAULT_PAGE, DEFAULT_PER_PAGE, orientation, null);
  }

  public UnsplashPhotoSearchResult searchPhotos(
      String query, int page, int perPage, UnsplashOrientation orientation, String[] collections)
      throws IOException {
    StringBuilder sb = new StringBuilder(searchPhotos);

    sb.append("?client_id=" + clientId);

    if (query != null && !query.isEmpty()) {
      sb.append("&query=" + query);
    }

    if (orientation != null) {
      sb.append("&orientation=" + orientation.getValue());
    }

    if (perPage < MIN_RESULTS_PER_PAGE && perPage > MAX_RESULTS_PER_PAGE) {
      throw new IllegalArgumentException(
          "\"per_page\" parameter must be a number between 1 and 30");
    }

    sb.append("&per_page=" + perPage);
    sb.append("&page=" + page);

    if (collections != null && collections.length != 0) {
      sb.append("&collections=" + getCollectionsAsString(collections));
    }

    URL url = new URL(sb.toString());
    return objectMapper.readValue(url, UnsplashPhotoSearchResult.class);
  }

  private String getCollectionsAsString(String[] collections) {
    StringBuilder sb = new StringBuilder();

    for (int i = 0; i < collections.length; i++) {
      String collection = collections[i];
      if (i == collections.length - 1) {
        sb.append(collection);
      } else {
        sb.append(collection + ",");
      }
    }

    return sb.toString();
  }

  /** photos of a collection * */
  public UnsplashImage[] getCollectionPhotos(String id) throws IOException {
    StringBuilder sb = new StringBuilder(collectionsUrl);

    if (id != null && !id.isEmpty()) {
      sb.append(id);
    }

    sb.append(photos);

    sb.append("?client_id=" + clientId);

    return objectMapper.readValue(new URL(sb.toString()), UnsplashImage[].class);
  }

  /** search collections * */
  public UnsplashCollectionSearchResult searchCollections(String query) throws IOException {
    return searchCollections(query, DEFAULT_PAGE, DEFAULT_PER_PAGE);
  }

  public UnsplashCollectionSearchResult searchCollections(String query, int page)
      throws IOException {
    return searchCollections(query, page, DEFAULT_PER_PAGE);
  }

  public UnsplashCollectionSearchResult searchCollections(String query, int page, int perPage)
      throws IOException {
    StringBuilder sb = new StringBuilder(searchCollections);

    sb.append("?client_id=" + this.clientId);
    sb.append("&query=" + query);
    sb.append("&page=" + page);
    sb.append("&perPage=" + perPage);

    return objectMapper.readValue(new URL(sb.toString()), UnsplashCollectionSearchResult.class);
  }
}
