package de.home.wallpaperdeamon.unsplash;

import lombok.Getter;

@Getter
public enum UnsplashOrientation {
    LANDSCAPE("landscape"),
    PORTRAIT("portrait"),
    SQUARISH("squarish");

    private String value;

    UnsplashOrientation(String value) {
        this.value = value;
    }
}
