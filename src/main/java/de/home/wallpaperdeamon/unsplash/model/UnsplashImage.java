package de.home.wallpaperdeamon.unsplash.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.HashMap;
import java.util.Map;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class UnsplashImage {
  private String id;
  private int height;
  private int width;
  private String description;
  private int likes;

  @JsonProperty("user")
  private UnsplashUser user;

  private Map<String, String> urls = new HashMap<>();
  private Map<String, String> links = new HashMap<>();
}
