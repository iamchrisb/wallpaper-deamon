package de.home.wallpaperdeamon.unsplash.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UnsplashCollectionSearchResult {
  private int total;
  private int total_pages;
  private UnsplashImage[] results;
}
