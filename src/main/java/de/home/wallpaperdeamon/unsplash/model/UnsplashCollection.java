package de.home.wallpaperdeamon.unsplash.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Map;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UnsplashCollection {
  private String id;
  private String title;
  private String description;

  @JsonProperty("published_at")
  private String publishedAt;

  @JsonProperty("updated_at")
  private String updatedAt;

  private boolean curated;
  private boolean featured;

  @JsonProperty("total_photos")
  private int totalPhotos;

  @JsonProperty("private")
  private boolean isPrivate;

  @JsonProperty("shared_key")
  private String sharedKey;

  private Map<String, String> links;

  private UnsplashUser user;
}
