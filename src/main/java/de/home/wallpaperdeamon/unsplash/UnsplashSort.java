package de.home.wallpaperdeamon.unsplash;

import lombok.Getter;

@Getter
public enum UnsplashSort {
    POPULAR("popular"),
    OLDEST("oldest"),
    LATEST("latest");

    private String value;

    UnsplashSort(String value) {
        this.value = value;
    }
}
